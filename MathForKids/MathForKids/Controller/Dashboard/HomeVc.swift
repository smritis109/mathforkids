//
//  HomeVc.swift
//  MathForKids

import UIKit

class HomeVc: UIViewController {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Home"
    }
    
    //MARK:- UIButton Actions

    @IBAction func btnAddtionTaped(_ sender: Any) {
        print("btnAddtionTaped")
        
        
        Operator.shared.selectedOperatorType = .addition

        let vcLevel =  CommonUtils.getVcObject(storyboardName:"Home" , vcIdentifier: "LevelsVc") as! LevelsVc
        self.navigationController?.pushViewController(vcLevel, animated: true)
    }
    @IBAction func btnMultiplicationTaped(_ sender: Any) {
        print("btnMultiplicationTaped")
        Operator.shared.selectedOperatorType = .multiplication
        let vcLevel =  CommonUtils.getVcObject(storyboardName:"Home" , vcIdentifier: "LevelsVc") as! LevelsVc
        self.navigationController?.pushViewController(vcLevel, animated: true)
    }
    @IBAction func btnDivisionTaped(_ sender: Any) {
        print("btnDivisionTaped")
        Operator.shared.selectedOperatorType = .division
        let vcLevel =  CommonUtils.getVcObject(storyboardName:"Home" , vcIdentifier: "LevelsVc") as! LevelsVc
        self.navigationController?.pushViewController(vcLevel, animated: true)
    }
    @IBAction func btnSubstrationTaped(_ sender: Any) {
        print("btnSubstrationTaped")
        Operator.shared.selectedOperatorType = .subtraction
        let vcLevel =  CommonUtils.getVcObject(storyboardName:"Home" , vcIdentifier: "LevelsVc") as! LevelsVc
        self.navigationController?.pushViewController(vcLevel, animated: true)
    }
    
}
