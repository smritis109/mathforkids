//
//  LevelVc+UITableView.swift
//  MathForKids
//
//  Created by Abhimanyu Rathore on 27/06/21.
//  Copyright © 2021 Smriti. All rights reserved.
//

import Foundation
import UIKit

extension LevelsVc : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {        
        switch Operator.shared.selectedOperatorType! {
        case .addition:return Operator.shared.addition.levelCount
        case .multiplication:return  Operator.shared.multiplication.levelCount
        case .subtraction:return  Operator.shared.subtraction.levelCount
        case .division:return  Operator.shared.subtraction.levelCount
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellConstants.levelCellIdentifier, for: indexPath) as! LevelCell
        if indexPath.row == 0 {
            cell.btnLock.isSelected = true
        }
        switch Operator.shared.selectedOperatorType! {
        case .addition: cell.lblLevel.text      = Operator.shared.addition.levels![indexPath.row].levelName
        case .multiplication:  cell.lblLevel.text      = Operator.shared.multiplication.levels![indexPath.row].levelName
        case .subtraction:  cell.lblLevel.text      = Operator.shared.subtraction.levels![indexPath.row].levelName
        case .division:  cell.lblLevel.text      = Operator.shared.division.levels![indexPath.row].levelName
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("table cell clicked buy user :- \(indexPath.row)")
        
        let vcQuestion =  CommonUtils.getVcObject(storyboardName:"Home" , vcIdentifier: "QuestionsVc") as! QuestionsVc
        vcQuestion.questions = Operator.shared.addition.levels![indexPath.row].listOfQuestions
        self.navigationController?.pushViewController(vcQuestion, animated: true)
        
    }
    
    
    
    
    
}
