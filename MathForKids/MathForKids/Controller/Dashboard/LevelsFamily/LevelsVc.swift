//
//  LevelsVc.swift
//  MathForKids


import UIKit


class LevelsVc: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    // MARK:- model object
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle()
    
        //registing LevelCell on table view
            self.tableView.register(UINib.init(nibName: "LevelCell", bundle: nil), forCellReuseIdentifier: CellConstants.levelCellIdentifier)
    }
}

//UI Related extension function
extension LevelsVc {
    
    func setNavigationTitle(){
        switch Operator.shared.selectedOperatorType {
        case .addition:
            self.title = "Addition"
        case .multiplication:
            self.title = "Multiplication"
        case .division:
            self.title = "Division"
        case .subtraction:
            self.title = "Subtraction"
        case .none:
            self.title  = ""
        }
    }
}

