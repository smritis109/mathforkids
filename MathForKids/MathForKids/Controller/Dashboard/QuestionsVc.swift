//
//  QuestionsVc.swift
//  MathForKids


import UIKit

class QuestionsVc: UIViewController {
    
    //UI Objects
    @IBOutlet weak var lblCurrentQuestion: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var btnNextSubmit: UIButton!
    
    

 
    // MARK: model object
     var questions:[LevelQuestion]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTopNAvigationBar()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.isHidden = false
    }
    
}

extension QuestionsVc:UITextFieldDelegate {
    
    //MARK:- Set Up Top Navigation Bar
    func setUpTopNAvigationBar(){
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK:- UITeUITextField Delegate function
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    //MARK:- UIButton Actions
    @IBAction func btnNextOrSubmitTaped(_ sender: Any) {
        print("next or submit taped")
    }
    @IBAction func btnCloseTaped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
