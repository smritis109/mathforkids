//
//  LoginVc.swift
//  MathForKids

import UIKit

class LoginVc: UIViewController,UITextFieldDelegate {
    
    //UI Objects
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtPassword: UITextField!
    
    // MARK: model object
    lazy var loginModel:LoginModel = {
        let model = LoginModel()
        return model
    }()
    
    // MARK: View Life Cycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnSigninTaped(_ sender: Any) {
        print("sign In tapped")
        
    }
    
    @IBAction func btnSignUPTaped(_ sender: Any) {
        print("sign UP tapped")
    }
    
    //MARK:- UITextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
    }

    func textFieldDidEndEditing(_ textField: UITextField) {

        print("textFieldDidEndEditing")
    }
}

