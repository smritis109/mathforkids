//
//  SignUpVc.swift
//  MathForKids


import UIKit

class SignUpVc: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    
    // MARK: model object
      lazy var signupModel:SignUpModel = {
          let model = SignUpModel()
          return model
      }()

    // MARK: View Life Cycle Functions
    override func viewDidLoad() {
          super.viewDidLoad()
      }
      override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
      }
    
    @IBAction func btnSignInClicked(_ sender: Any) {
        print("btnSignInClicked")
    }
    
    
    @IBAction func btnSignUpClicked(_ sender: Any) {
        print("btnSignUpClicked")
    }

    //MARK:- UITextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        print("textFieldDidEndEditing")
    }

}
