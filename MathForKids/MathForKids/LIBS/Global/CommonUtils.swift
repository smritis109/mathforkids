//
//  UIViewControllerExtension.swift
//  MathForKids
//
//  Created by Abhimanyu Rathore on 27/06/21.
//  Copyright © 2021 Smriti. All rights reserved.
//

import Foundation
import UIKit


class CommonUtils{
    
    static func getVcObject(storyboardName:String,vcIdentifier:String)->UIViewController {
        let storyboard = UIStoryboard.init(name: storyboardName, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: vcIdentifier)
        return vc
    }
}

