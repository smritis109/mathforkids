//
//  Macors.swift
//  MathForKids
//
//  Created by Nidhi Singh Naruka on 29/11/20.
//  Copyright © 2020 Smriti. All rights reserved.
//

import Foundation
import  UIKit

//Some Global values
let levelScoreLimit = 85 //in percentage
let timeLimitOfEachLevel:TimeInterval = 600 //10 minute 10*60 = 600


//UI Helper
let screenWidth = UIScreen.main.bounds.size.width
let screenHeight = UIScreen.main.bounds.size.height


